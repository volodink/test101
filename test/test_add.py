import pytest
from src.add import add

testdata = [(1.1, 1.1, 2.2), 
            ([1,2], [3,4], [1,2,3,4]) , 
            ([],[],[]),
            (41,1,42), 
            (1,2,3),
            (100,100,200)];

@pytest.mark.parametrize("a,b,result", testdata)
def test_add(a, b, result):
    assert add(a, b) == result
