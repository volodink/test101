all: clean tests
	echo "all"

tests:
	virtualenv venv
	. venv/bin/activate && pip3 install -r requirements.txt && pytest -v && deactivate

clean:
	rm -rf venv

